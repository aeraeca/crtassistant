from pathlib import Path
import json
from datetime import datetime
import re

from pymediainfo import MediaInfo

from ..utils.formats import VIDEO_FORMATS, AUDIO_FORMATS, CONTAINER_FORMATS


class VideoInfo():
    def __init__(self, video_file: Path, temp_dir: Path) -> None:
        """Generates and stores mediainfo metadata about a given video

        Args:
            video_file (Path): Single video file to process
            temp_dir (Path): Temporary storage location

        Raises:
            RuntimeError: If mediainfo can not parse, likely meaning it cannot locate the
                library on the system
        """        
        self._video_file = video_file
        if MediaInfo.can_parse():
            self.temp_dir = temp_dir
            self._info_string_raw = str(MediaInfo.parse(self._video_file, output="STRING", full=False, mediainfo_options={'inform_version' : '1'}))
            self._info_dict = json.loads(MediaInfo.parse(self._video_file, output="JSON")) # type: ignore
            self._parse_relevant_info()
            self._get_site_mediainfo()
        else:
            raise RuntimeError('Cannot locate mediainfo library on system')

    @property
    def full_str(self) -> str:
        return self._info_string_parsed

    @property
    def full_dict(self) -> dict:
        return self._info_dict

    @property
    def tagging_info(self) -> dict:
        return self._relevant_info
    
    def _parse_relevant_info(self) -> None:
        """Builds a dict with the parts of the mediainfo dump we are interested in
        """        
        self._relevant_info = {
            'general': {},
            'video': {},
            'audio': [],
            'text': []
        }
        
        for track in self.full_dict['media']['track']:
            try:
                track_type = track['@type']
                if track_type == 'General':
                    self._relevant_info['general'].update({
                        'filename': track['FileName'],
                        'length': float(track['Duration']),
                        'size': track['FileSize_String'],
                        'format': f'{CONTAINER_FORMATS[track["Format"]]}.container' if track['Format'] in CONTAINER_FORMATS else None
                    })
                    if 'extra' in track:
                        self._relevant_info['general'].update({
                            'imdb': track['extra']['IMDB'] if 'IMDB' in track['extra'] else None,
                            'tmdb': track['extra']['TMDB'] if 'TMDB' in track['extra'] else None,
                            'TVDB': track['extra']['TVDB'] if 'TVDB' in track['extra'] else None
                        })
                    else:
                        self._relevant_info['general'].update({
                            'imdb': None,
                            'tmdb': None,
                            'TVDB': None
                        })
                elif track_type == 'Video':
                    self._relevant_info['video'].update({
                        'codec': VIDEO_FORMATS[track['CodecID']] if 'CodecID' in track else VIDEO_FORMATS[track['Format_Commercial']],
                        'language': track['Language_String2'] if 'Language_String2' in track else None,
                        'language_string': track['Language_String'].replace(' ', '.').lower() if 'Language_String' in track else None,
                        'width': int(track['Width']),
                        'height': int(track['Height']),
                        'hdr': True if 'HDR Format' in track and 'HDR10 Compatible' in track['HDR format'] else False,
                        'hdr10plus': True if 'HDR Format' in track and 'HDR10+' in track['HDR format'] else False,
                        'dv': True if 'HDR Format' in track and 'Dolby Vision' in track['HDR format'] else False
                    })
                elif track_type == 'Audio':
                    self._relevant_info['audio'].append({
                        'codec': AUDIO_FORMATS[track['Format']] if track['Format'] != 'MPEG Audio' else AUDIO_FORMATS[track['Format_Profile']],
                        'language': track['Language_String2'] if 'Language_String2' in track else None,
                        'language_string': track['Language_String'].replace(' ', '.').lower() if 'Language_String' in track else None,
                        'channels': f'{track["Channels"]}.channels' if int(track['Channels']) > 2 else ('stereo' if int(track['Channels']) == 2 else 'mono')
                    })
                elif track_type == 'Text':
                    self._relevant_info['text'].append({
                        'language': track['Language_String2'] if 'Language_String2' in track else None,
                        'language_string': track['Language_String'].replace(' ', '.').lower() if 'Language_String' in track else None
                    })
            except (KeyError, ValueError) as err:
                print(f'Error in parsing track {track["@type"]}: {err}')
                continue

        self._relevant_info['video'].update({
            'common_resolution': self._get_common_resolution()
        })

    def _get_common_resolution(self) -> str:
        """Parses height and width to determine the common resolution name

        Returns:
            str: One of: 2160p, 1080p, 720p, standard.definition
        """   
        if 'width' in self._relevant_info['video'] and 'height' in self._relevant_info['video']:
            width = self._relevant_info['video']['width']
            height = self._relevant_info['video']['height']

            if width > 1920 or height > 1080:
                return '2160p'
            elif width > 1280 or height > 720:
                return '1080p'
            elif width > 720 or height > 576:
                return '720p'
            else:
                return 'standard.definition'
        else:
            return ''

    def _get_site_mediainfo(self) -> None:
        """Gets a site friendly string representation of the mediainfo for use in the
            description
        """        
        temp_mediainfo = self.temp_dir / str(int(datetime.now().timestamp()))
        with temp_mediainfo.open('w', newline="", encoding='utf-8') as fo:
            fo.write(self._info_string_raw)
        self._info_string_parsed = ''
        with temp_mediainfo.open('r', encoding='utf8') as fo2:
            for line in fo2.readlines():
                if not re.match('^ReportBy', line):
                    if re.match(r'^Complete\ name', line):
                        self._info_string_parsed += f'Complete name                            : {Path(line.split(":")[-1].strip()).name}\n'
                    else:
                        self._info_string_parsed += line
        temp_mediainfo.unlink()

if __name__ == '__main__':
    pass
