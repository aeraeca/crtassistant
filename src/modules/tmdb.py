from datetime import datetime
from typing import List, Union

import requests
import urllib.parse

from ..utils.formats import TMDB_MOVIE_GENRES, TMDB_TV_GENRES

class TMDB():
    def __init__(self, cat: str, key: str, id: int = 0, search: str = '', year: int = 0, seasons: List[int] = [], language: str = '') -> None:
        """Queries TMDB API by either string or ID search and holds results

        Args:
            cat (str): The category to search. Can only be 'movie' or 'tv'
            key (str): TMDB API key
            id (int, optional): TMDB ID to search. Defaults to 0.
            search (str, optional): Search string to use. Defaults to ''.
            year (int, optional): Year to search for, used when performing a search string.
                Defaults to 0.
            seasons (List[int], optional): List of seasons in the format of
                [first, last], [single], or [], for determining TV years. Defaults to [].
            language (str, optional): Language of the content to search for. Defaults to ''.

        Raises:
            TypeError: If cat is not 'movie' or 'tv'
            ValueError: If seasons are provided in a invalid format
        """        
        if isinstance(cat, str) and cat in ['movie', 'tv']:
            self._cat = cat
        else:
            raise TypeError('Category can only be movie or tv')

        if len(seasons) in range(3):
            self._get_seasons = seasons
        else:
            raise ValueError(
                f'Provided seasons invalid: {seasons}')

        self._tmdb_id = id
        self._search = search
        self._year = year
        self._search_language = language

        self._key = f'?api_key={key}'
        self._base_url = 'https://api.themoviedb.org/3'
        self._include_adult_query = '&include_adult=false'
        self._language_query = '&language=en-US'
        self._additional_info_query = '&append_to_response=alternative_titles,external_ids'

    @property
    def tmdb_id(self) -> int:
        return self._tmdb_id
    
    @tmdb_id.setter
    def tmdb_id(self, id: int) -> None:
        if isinstance(id, int) and id > 0:
            self._tmdb_id = id
        else:
            raise ValueError(f'TMDB ID must be an integer greater zero, {id} was provided')

    def do_initial_search(self) -> None:
        """Run the initial search, by ID if provided, or search string if not

        Raises:
            RuntimeError: If neither an ID or search string was provided
        """        
        if isinstance(self.tmdb_id, int) and self.tmdb_id > 0:
            self.get_by_id()
        elif isinstance(self._search, str) and self._search != '':
            self._get_by_search()
        else:
            raise RuntimeError('No id or search query provided for TMDB query')

    def get_by_id(self) -> None:
        """Get media information by querying ID

        Raises:
            ValueError: If the API response is anything other than 200
        """        
        res = requests.get(
            f'{self._base_url}/{self._cat}/{self.tmdb_id}{self._key}{self._language_query}{self._additional_info_query}')

        if res.status_code != 200:
            raise ValueError('Bad response from tmdb')

        self._parse_response(res)

    def _get_by_search(self) -> None:
        """Attempts a search based language and query for TV, and year and query for Movies.
            Returns the first result.

        Raises:
            ValueError: If response status is anything other than 200
            LookupError: If no results were returned from the search
            LookupError: If no results were returned from the search
        """        
        if self._cat == 'tv' or self._year == 0:
            res = requests.get(
                f'{self._base_url}/search/{self._cat}{self._key}{self._language_query}&page=1{self._include_adult_query}&query={urllib.parse.quote(self._search)}')
        else:
            res = requests.get(
                f'{self._base_url}/search/{self._cat}{self._key}{self._language_query}&page=1{self._include_adult_query}&query={urllib.parse.quote(self._search)}&year={self._year}')

        if res.status_code != 200:
            raise ValueError('Bad response from tmdb')

        parsed = res.json()
        results = parsed['results']
        if len(results) > 0:
            if self._cat == 'tv' and self._search_language != '':
                tmdb_id = next(
                    (x['id'] for x in results if x['original_language'] == self._search_language), None)
                if self.tmdb_id is None:
                    raise LookupError(
                        f'Could not find any suitable search search results for {self._search} in category {self._cat}')
                else:
                    self.tmdb_id = tmdb_id  # type: ignore            
            else:
                self.tmdb_id = results[0]['id']
            self.get_by_id()
        else:
            raise LookupError(
                f'Could not find any suitable search search results for {self._search} in category {self._cat}')

    def _parse_response(self, response: requests.Response) -> None:
        """Parsed the API response and store information we are interested in

        Args:
            response (requests.Response): TMDB API response for item by ID
        """        
        parsed = response.json()
        self._tmdb_url = f'https://www.themoviedb.org/{self._cat}/{self.tmdb_id}'
        self._poster_url = f'https://image.tmdb.org/t/p/original{parsed["poster_path"]}' if parsed['poster_path'] is not None else None
        self._description = parsed['overview']
        self._original_language = parsed['original_language']
        self._alternate_title = next(
            (x['title'] for x in parsed['alternative_titles']['results' if self._cat == 'tv' else 'titles'] if x['iso_3166_1'] == 'US'), None)
        self.genres = [x['name'] for x in parsed['genres']]

        if self._original_language != 'en':
            self._languages = [x['english_name']
                               for x in parsed['spoken_languages'] if x['english_name'] != 'English']

        imdb = parsed['external_ids']['imdb_id']
        self._imdb_url = f'https://www.imdb.com/title/{imdb}' if imdb is not None else None

        if self._cat == 'movie':
            self.year = datetime.fromisoformat(parsed['release_date']).year
            self.title = parsed['title']
            self._original_title = parsed['original_title']
            self.total_number_of_seasons = None
            self.last_aired = None
            self._tvdb_url = None
            self.runtime = parsed['runtime']
            self._tvdb_url = None
            self._status = None
            self._genres = [TMDB_MOVIE_GENRES[x['id']] for x in parsed['genres']]

        if self._cat == 'tv':
            self.year = self._parse_seasons(parsed['seasons'])
            self.last_aired = datetime.fromisoformat(
                parsed['last_air_date']).year
            self.title = parsed['name']
            self._original_title = parsed['original_name']
            self.total_number_of_seasons = parsed['number_of_seasons']
            self.runtime = None
            tvdb = parsed['external_ids']['tvdb_id']
            self._tvdb_url = f'https://www.thetvdb.com/dereferrer/series/{tvdb}' if tvdb is not None else None
            self._status = parsed['status']
            self._genres = [TMDB_TV_GENRES[x['id']] for x in parsed['genres']]

    def _parse_seasons(self, seasons: dict) -> Union[str, None]:
        """Returns a string representing eith a single year or range of years that TV
            seasons were aired on

        Args:
            seasons (dict): Object parsed from the TMDB season information for a show

        Returns:
            Union[str, None]: string representing a single or range of seasons, or None
                if we aren't expecting one
        """        
        if self._get_seasons == [-1]:
            first_year = datetime.fromisoformat(seasons[0]['air_date']).year
            last_year = datetime.fromisoformat(seasons[-1]['air_date']).year
            if first_year == last_year:
                return str(first_year)
            else:
                return f'{first_year}-{last_year}'
        elif len(self._get_seasons) == 1:
            return str(next((datetime.fromisoformat(x['air_date']).year for x in seasons if x['season_number'] == self._get_seasons[0]), 0))
        elif len(self._get_seasons) == 2:
            first_year = next((datetime.fromisoformat(
                x['air_date']).year for x in seasons if x['season_number'] == self._get_seasons[0]), 0)
            last_year = next((datetime.fromisoformat(
                x['air_date']).year for x in seasons if x['season_number'] == self._get_seasons[1]), 0)
            if first_year == last_year:
                return str(first_year)
            else:
                return f'{first_year}-{last_year}'

    def get_info(self) -> dict:
        """Returns useful information collected about a given piece of media

        Returns:
            dict: Relevant attributes that will be neede later
        """        
        return {
            'category': self._cat,
            'tmdb_id': self.tmdb_id,
            'tmdb_url': self._tmdb_url,
            'imdb_url': self._imdb_url,
            'tvdb_url': self._tvdb_url,
            'title': self.title,
            'description': self._description,
            'year': self.year,
            'number_of_seasons': self.total_number_of_seasons,
            'original_language': self._original_language,
            'alternate_title': self._alternate_title,
            'original_title': self._original_title,
            'runtime': self.runtime,
            'status': self._status,
            'poster_url': self._poster_url,
            'genres': self._genres
        }


if __name__ == '__main__':
    pass
