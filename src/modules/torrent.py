from pathlib import Path
from datetime import datetime
from typing import List

from torf import Torrent


def create_torrent(media: Path, out_dir: Path, announce_key: str, threads: int, cb, ignore_globs: List[str]) -> str:
    """Creates and writes out a torrent

    Args:
        media (Path): Path to media to hash
        out_dir (Path): The directory to write out to
        announce_key (str): CRT announce key to use
        threads (int): Number of threads to use when hashing
        cb (function): Callback function for progress bar
        ignore_globs (List[str]): List of globs to ignore when hashing torrent.

    Raises:
        RuntimeError: If there was some unspecified error when hashing the torrent

    Returns:
        str: The name of the created file
    """
    announce = f'https://signal.cathode-ray.tube/{announce_key}/announce'
    media_name = media.name
    out_file = out_dir / f'{media_name}.torrent'
    torrent = Torrent(path=media, name=media.name, trackers=announce, private=True,
                      source='CRT', created_by='crt-assistant', piece_size=None, 
                      creation_date=datetime.now(), exclude_globs=ignore_globs)
    success = torrent.generate(threads=threads, callback=cb, interval=1)
    if success:
        torrent.write(out_file)
    else:
        raise RuntimeError('There was an issue generating the torrent')

    return out_file.name


if __name__ == '__main__':
    pass
